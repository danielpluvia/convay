# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to connect to mysql remotely
1. configure /config/database.yml   
	* add the followig two lines under default:    
	* `$ host: 127.0.0.1`  
  	* `$ port: 3307`  
2. connect once to remote host  
	* in terminal:  
	* `$ ssh -i key_file ubuntu@115.146.85.17` 
	* then type in password 111111  
3. establish ssh tunnel for remote mysql access
	* in terminal:
	* `$ ssh -L 127.0.0.1:3307:127.0.0.1:3306 ubuntu@115.146.85.17 -N`
4. migrate database from rails
	* `$ rake db:create`
	* `$ rake db:migrate`
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact