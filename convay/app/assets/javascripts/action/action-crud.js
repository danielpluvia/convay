/**
 * Created by lyy on 9/8/15.
 */
$(document).on('ready page:change',function() {
    domCommEvents.setListener();
});
$(document).on('ready page:load',function() {
    domCommEvents.setListener();
});

var domCommEvents = {
    setListener: function() {
        this.editActionEvent();
    },
    editActionEvent: function() {
        $("#submit-action-button").unbind();
        $("#submit-action-button").on("click", {}, dommCommEventsProcessor.processEditActionEvent);
    }
}

var dommCommEventsProcessor = {
    processEditActionEvent: function(event) {
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-action-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/ctransactions/" + $("input[id='caction_ctransaction_id']").attr("value") + '/cactions/' + $("input[id='caction_ctransaction_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        } else if(operation == 'new') {
            href = "/ctransactions/" + $("input[id='caction_ctransaction_id']").attr("value") + '/cactions';
            commModule.sendData(href, datum, 'POST');
        }
    },

}

var dommCommEventsCallBack = {
    createActionEventSuccessCallBack: function(result) {
        // Close modal.
        $("#create-action-modal").modal("hide");
        helper.createNewActionInTable(result);
        helper.showNotification("Create Successfully", "You have created a new action! Take a look.");
    },
    createActionEventFailCallBack: function(result) {
        result= $.parseJSON(result.responseText);
        console.log(result);
        // Remove old errors.
        $("#new_caction .action-form-error").text("");
        // Fill in new errors.
        for(var key in result) {
            $("#" + key + "_error").text(result[key][0]);
        }
    },
    editActionEventSuccessCallBack: function(result) {
        // Close modal.
        $("#edit-action-modal").modal("hide");
        helper.showNotification("Update Successfully", "You have edited client with");
    },
    editActiontEventFailCallBack: function(result) {
        $("#edit-action-modal").modal("hide");
        helper.showNotification("Update Failed", "You have edited client with id: " + window.currentClient + " ! Take a look.");
    },
    deleteActionEventSuccessCallBack: function(result) {
        // Close modal.
        $("#delete-action-modal").modal("hide");
        //helper.updateRowWithClient(result);
        helper.showNotification("Delete Successfully", "You have deleted action with id: " + window.currentAction + " ! Take a look.");
        helper.deleteRowByActionId(result);
    },
    deleteActionEventFailCallBack: function(result) {
        // Close modal.
        $("#delete-action-modal").modal("hide");
        helper.showNotification("Delete Failed", "You have failed to delete action with id: " + window.currentAction + " ! Take a look.");

    },
}

var commModule = {
    postData: function(href, datum, successCallback, failCallback) {
        $.post(href, datum)
            .success(successCallback)
            .fail(failCallback);
    },
    putData: function(href, datum, successCallback, failCallback) {
        $.ajax({
            url: href,
            type: 'PUT',
            success: successCallback,
            error: failCallback,
            data: datum
        });
    },
    deleteData: function(href, datum, successCallback, failCallback) {
        $.ajax({
            url: href,
            type: 'DELETE',
            success: successCallback,
            error: failCallback,
            data: datum
        });
    },
    sendData: function(href, datum, method) {
        $.ajax({
            url: href,
            type: method,
            data: datum
        });
    }
}

var helper = {
    refresh: function() {
        window.currentAction = null;
        window.currentPeration = null;
        domCommEvents.setListener();
        domPresentationEvents.setListener();
    },
    genaralSuccessNotification: function (result) {
        alert(result);
        console.log(result);
    },
    genaralFailNotification: function () {
        alert("Failed Updated!");
    },
    getObjectIdbyName: function(name) {
        return name.split("-").pop();
    },
    createNewActionInTable: function(action) {
        console.log(action);
        var newActionRow =  $("#hidden-action-row").clone();
        newActionRow.find("span.task-title-sp").text(action.action_name);
        newActionRow.show();
        newActionRow.find("div").show();
        newActionRow.attr("id", "action-row-" + action.id);
        newActionRow.appendTo("#sortable");
        this.refresh();
    },

    deleteRowByActionId: function(action) {
        var actionRow = $("#action-row-" + action.id);
        actionRow.remove();
        this.refresh();
    },
    showNotification: function(title, notification) {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: notification,
            // (string | optional) the image to display on the left
            image: '/assets/panel/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: Date.now(),
        });
    }
}

var url = {
    show: "/clients",
    create: "/clients",
    edit: "/clients/",
    delete: "/clients/"
}/**
 * Created by lyy on 9/16/15.
 */
