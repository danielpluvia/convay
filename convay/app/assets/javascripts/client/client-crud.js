/**
 * Created by lyy on 9/8/15.
 */
$(document).on('ready page:change',function() {
    domCommEvents.setListener();
});


var domCommEvents = {
    setListener: function() {
        this.editClientEvent();
    },
    editClientEvent: function() {
        $("#submit-client-button").unbind();
        $("#submit-client-button").on("click", {}, dommCommEventsProcessor.processEditClientEvent);
    },

}

var dommCommEventsProcessor = {
    processEditClientEvent: function(event) {
        // Get the button id from the previous event.
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-client-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/clients/" + $("input[id='client_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        } else if(operation == 'new') {
            href = "/clients";
            commModule.sendData(href, datum, 'POST');
        }

    },
}

var commModule = {
    sendData: function(href, datum, method) {
        $.ajax({
            url: href,
            type: method,
            data: datum
        });
    }
}

var helper = {
    appendCSRF: function(datum) {
        datum.authenticity_token = $("meta[name='csrf-token']").attr("content");
        return datum;
    },
    genaralSuccessNotification: function (result) {
        alert(result);
        console.log(result);
    },
    genaralFailNotification: function () {
        alert("Failed Updated!");
    },
    getObjectIdbyName: function(name) {
        return name.split("-").pop();
    },
    showNotification: function(title, notification) {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: notification,
            // (string | optional) the image to display on the left
            image: '/assets/panel/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: Date.now(),
        });
    }
}