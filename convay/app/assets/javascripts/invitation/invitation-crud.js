/**
 * Created by lyy on 9/12/15.
 */
/**
 * Created by lyy on 9/8/15.
 */
$(document).on('ready page:change',function() {
    domCommEvents.setListener();
    domPresentetionEvent.setListener();
});

$(document).on('ready page:load',function() {
    domCommEvents.setListener();
    domPresentetionEvent.setListener();


        /**
         * When the send message link on our home page is clicked
         * send an ajax request to our rails app with the sender_id and
         * recipient_id
         */

        console.log("dsadas");
        $('.start-conversation').click(function (e) {
            console.log("in");
            e.preventDefault();
            var sender_id = $(this).data('sid');
            var recipient_id = $(this).data('rip');
            var datum = { sender_id: sender_id, recipient_id: recipient_id };
            datum = helper.appendCSRF(datum);
            $.post("/conversations", datum, function (data) {
                chatBox.chatWith(data.conversation_id);
            });
        });



        /**
         * Used to minimize the chatbox
         */

        $(document).on('click', '.toggleChatBox', function (e) {
            e.preventDefault();

            var id = $(this).data('cid');
            chatBox.toggleChatBoxGrowth(id);
        });

        /**
         * Used to close the chatbox
         */

        $(document).on('click', '.closeChat', function (e) {
            e.preventDefault();

            var id = $(this).data('cid');
            chatBox.close(id);
        });


        /**
         * Listen on keypress' in our chat textarea and call the
         * chatInputKey in chat.js for inspection
         */

        $(document).on('keydown', '.chatboxtextarea', function (event) {

            var id = $(this).data('cid');
            chatBox.checkInputKey(event, $(this), id);
        });

        /**
         * When a conversation link is clicked show up the respective
         * conversation chatbox
         */

        $('a.conversation').click(function (e) {
            e.preventDefault();

            var conversation_id = $(this).data('cid');
            chatBox.chatWith(conversation_id);
        });

        /*
         *  Trigger by browser. In order to connect all the convo.
         * */
        $('.start-conversation').dblclick(function (e) {
            e.preventDefault();
            var sender_id = $(this).data('sid');
            var recipient_id = $(this).data('rip');
            var datum = { sender_id: sender_id, recipient_id: recipient_id };
            datum = helper.appendCSRF(datum);
            $.post("/conversations", datum, function (data) {
                chatBox.chatWith(data.conversation_id);
                chatBox.close(data.conversation_id);
            });
        });

        /*
         *   Click the notification button, UI changed.
         * */

        $(".click-notification").click(function(event) {
            // Change notification number.
            $(".notification-number").each(function() {
                var currentElement = $(this);
                var number = parseInt(currentElement.text());
                currentElement.text(number-1);
            });

            // Remove a notification in panel.
            $(event.target).closest("li").remove();

        });

});

var domPresentetionEvent = {
    setListener: function() {
        this.sideBarMenu();
    },
    sideBarMenu: function() {
        $("a.side-bar-menu").click(function() {
            console.log("dsad");
            $("a.side-bar-menu").removeClass("active");
            $(this).addClass("active");
        });
    }
}

var domCommEvents = {
    setListener: function() {
        this.editInvitationEvent();
        this.editTransactionEvent();
        this.editActionEvent();
        this.editClientEvent();
    },
    editTransactionEvent: function() {
        $("#submit-transaction-button").unbind();
        $("#submit-transaction-button").on("click", {}, dommCommEventsProcessor.processEditTransactionEvent);
    },

    editInvitationEvent: function() {
        $("#submit-invitation-button").unbind();
        $("#submit-invitation-button").on("click", {}, dommCommEventsProcessor.processEditInvitationEvent);
    },
    editActionEvent: function() {
        $("#submit-action-button").unbind();
        $("#submit-action-button").on("click", {}, dommCommEventsProcessor.processEditActionEvent);
    },
    editClientEvent: function() {
        $("#submit-client-button").unbind();
        $("#submit-client-button").on("click", {}, dommCommEventsProcessor.processEditClientEvent);
    },
};

var dommCommEventsProcessor = {
    processEditInvitationEvent: function(event) {
        console.log("dsadasdas");
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-invitation-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/invitations/" + $("input[id='invitation_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        } else if(operation == 'new') {
            href = "/invitations";
            commModule.sendData(href, datum, 'POST');
        }

    },
    processEditActionEvent: function(event) {
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-action-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/ctransactions/" + $("input[id='caction_ctransaction_id']").attr("value") + '/cactions/' + $("input[id='caction_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        } else if(operation == 'new') {
            href = "/ctransactions/" + $("input[id='caction_ctransaction_id']").attr("value") + '/cactions';
            commModule.sendData(href, datum, 'POST');
        }
    },
    processEditTransactionEvent: function(event) {
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-transaction-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/ctransactions/" + $("input[id='ctransaction_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        }
    },
    processEditClientEvent: function(event) {
        // Get the button id from the previous event.
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-client-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/clients/" + $("input[id='client_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        } else if(operation == 'new') {
            href = "/clients";
            commModule.sendData(href, datum, 'POST');
        }

    },
};


var commModule = {
    sendData: function(href, datum, method) {
        $.ajax({
            url: href,
            type: method,
            data: datum
        });
    }
};

var helper = {
    genaralSuccessNotification: function (result) {
        alert(result);
        console.log(result);
    },
    genaralFailNotification: function () {
        alert("Failed Updated!");
    },
    getObjectIdbyName: function(name) {
        return name.split("-").pop();
    },
    createNewClentInTable: function() {
        return $("#client-row-hidden").clone();
    },
    deleteRowBySentInvitationId: function(invitation) {
        var invitationRow = $("#sent-invitation-row-" + invitation.id);
        invitationRow.remove();
    },
    showNotification: function(title, notification) {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: notification,
            // (string | optional) the image to display on the left
            image: '/assets/panel/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: Date.now(),
        });
    },
    appendCSRF: function(datum) {
        datum.authenticity_token = $("meta[name='csrf-token']").attr("content");
        return datum;
    }
}
