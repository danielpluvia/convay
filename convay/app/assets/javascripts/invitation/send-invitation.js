/**
 * Created by lyy on 9/11/15.
 */

$(document).ready(function() {
    $(".js-example-basic-single").select2({
        placeholder: "Select a value"
    });
    domCommEvents.setListener();
});


var domCommEvents = {
    setListener: function() {
        this.companyChange();
    },
    companyChange: function() {
        //$("#invitation_company").unbind();
        $("#invitation_company").on("select2:select", {}, dommCommEventsProcessor.processCompanyChange);
    },
}

var dommCommEventsProcessor = {
    processCompanyChange: function(event) {
       // alert("here");
        var chosenCompanyId = $(event.target).val();
        var href = '/companies/' + chosenCompanyId + '/' + 'users';
        var datum = {};
        commModule.getData(href, datum, dommCommEventsCallBack.companyChangeSuccessCallback, dommCommEventsCallBack.companyChangeFailCallback);
    },

}

var dommCommEventsCallBack = {
    companyChangeSuccessCallback: function(result) {
        // Clear all the user information of the previous company.
        $("#invitation_receiver_id option").remove();
        //$("#select2-invitation_receiver-container").text("");
        $("#invitation_receiver_id").select2("val", "");
        if(result.length > 0) {
            // Append new receivers information.
            for(var index in result) {
                $("#invitation_receiver_id").append($('<option>', {
                    value: result[index].id,
                    text : result[index].email
                }));
            }
        }
        $("#invitation_receiver_id").trigger("change");
       // alert($("#invitation_receiver").val());
        console.log(result);
    },
    companyChangeFailCallback: function(result) {
        console.log(result);
    },
}

var commModule = {
    getData: function(href, datum, successCallback, failCallback) {
        $.get(href, datum)
            .success(successCallback)
            .fail(failCallback);
    },
    postData: function(href, datum, successCallback, failCallback) {
        $.post(href, datum)
            .success(successCallback)
            .fail(failCallback);
    },
    putData: function(href, datum, successCallback, failCallback) {
        $.ajax({
            url: href,
            type: 'PUT',
            success: successCallback,
            error: failCallback,
            data: datum
        });
    },
    deleteData: function(href, datum, successCallback, failCallback) {
        $.ajax({
            url: href,
            type: 'DELETE',
            success: successCallback,
            error: failCallback,
            data: datum
        });
    }
}

var helper = {
    genaralSuccessNotification: function (result) {
        alert(result);
        console.log(result);
    },
    genaralFailNotification: function () {
        alert("Failed Updated!");
    },
    getObjectIdbyName: function(name) {
        return name.split("-").pop();
    },
    deleteRowByClientId: function(client) {
        var clientRow = $("#client-row-" + client.id);
        clientRow.remove();
    },
    showNotification: function(title, notification) {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: notification,
            // (string | optional) the image to display on the left
            image: '/assets/panel/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: Date.now(),
        });
    }
}

var url = {
    show: "/invitations",
    create: "/clients",
    edit: "/clients/",
    delete: "/clients/"
}