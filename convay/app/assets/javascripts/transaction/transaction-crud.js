$(document).on('ready page:change',function() {
    domCommEvents.setListener();
});


var domCommEvents = {
    setListener: function() {
        this.editTransactionEvent();
    },
    editTransactionEvent: function() {
        $("#submit-transaction-button").unbind();
        $("#submit-transaction-button").on("click", {}, dommCommEventsProcessor.processEditTransactionEvent);
    },
};

var dommCommEventsProcessor = {
    processEditTransactionEvent: function(event) {
        var datum = $("#modal-form").serialize();
        var operation = $("#submit-transaction-button").attr("operation");
        var href = '';
        if(operation == 'edit'){
            href = "/ctransactions/" + $("input[id='ctransaction_id']").attr("value");
            commModule.sendData(href, datum, 'PUT');
        }
    },
};


var commModule = {
    sendData: function(href, datum, method) {
        $.ajax({
            url: href,
            type: method,
            data: datum
        });
    }
};

var helper = {
    genaralSuccessNotification: function (result) {
        alert(result);
        console.log(result);
    },
    genaralFailNotification: function () {
        alert("Failed Updated!");
    },
    getObjectIdbyName: function(name) {
        return name.split("-").pop();
    },
    createNewClentInTable: function() {
        return $("#client-row-hidden").clone();
    },
    deleteRowBySentInvitationId: function(invitation) {
        var invitationRow = $("#sent-invitation-row-" + invitation.id);
        invitationRow.remove();
    },
    showNotification: function(title, notification) {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: notification,
            // (string | optional) the image to display on the left
            image: '/assets/panel/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: Date.now(),
        });
    },
    appendCSRF: function(datum) {
        datum.authenticity_token = $("meta[name='csrf-token']").attr("content");
        return datum;
    }
}
