class CompaniesController < ApplicationController
	before_action :authenticate_user!
	# before_action :authenticate_owner!

	def index
		@companies = Company.all
	end

	def show
		
	end

	def new
		@company = Company.new
	end

	def edit
		
	end

	def create
		@company = Company.new(company_params)
		if @company.save
			redirect_to companies_path
		else
			render 'new'
		end
	end

	def destroy
		@company = Company.find(params[:id])
		@company.destroy
		redirect_to companies_path
	end

	# GET /companies/:company_id/users(.:format)
	def users
		company = Company.find(params[:company_id])
		render :json => company.users
	end

	private
		# def authenticate_owner!
		# 	unless @client.users.include?(current_user)
		# 		redirect_to welcome_index_path
		# 	end
		# end

		def company_params
			params.require(:company).permit(:name, :contact, :address)
		end
end
