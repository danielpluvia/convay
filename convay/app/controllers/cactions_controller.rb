class CactionsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_ctransaction
  before_action :authenticate_owner!, except: [:index, :new, :create]

  layout 'base_panel', :only => :index

  def index
		@cactions = @ctransaction.cactions
    @temp_new_caction = Caction.new
    @shared_actions = @ctransaction.shared_with_transaction.cactions if @ctransaction.shared_with_transaction
	end

	def show
		
	end

	def new
		@caction = Caction.new
	end

	def edit
    @caction = Caction.find params[:id]
	end

	def create
    @caction = Caction.new(caction_params)
    @caction.save
	end

	def update
    @caction = Caction.find params[:id]
    @update_success = @caction.update caction_params
	end

	def destroy
    @caction = Caction.find params[:id]
    @destroy_success = @caction.destroy
	end

	private
		def set_ctransaction
      @ctransaction = Ctransaction.find params[:ctransaction_id]
		end

		def caction_params
			params.require(:caction).permit(:action_name, :id, :state, :ctransaction_id)
		end

    def authenticate_owner!
      unless @ctransaction.user == current_user
        redirect_to welcome_index_path
      end
    end

end
