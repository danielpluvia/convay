class ClientsController < ApplicationController
	 before_action :authenticate_user!
   before_action :set_client, except: [:index, :create, :new]
   before_action :authenticate_owner!, except: [:index, :create, :new]
	 layout 'base_panel', :except => [:edit, :new, :show, :create, :update, :destroy]

	def index
		@clients = current_user.clients
	end

	def show
   # @client = Client.find params[:id]
	end

  def new
    @client = Client.new
  end

	def edit
   # @client = Client.find params[:id]
	end

	def create
		@client = Client.new(client_params)
		if @client.valid?
			@client.user_id = current_user.id
			@client.save
    end
	end

	def update
	#	@client = Client.find params[:id]
		@update_success = @client.update client_params
	end

	def destroy
		#@client = Client.find params[:id]
    @destroy_success = @client.destroy
  end

	private
		def set_client
			@client = Client.find(params[:id])
		end

		def client_params
			params.require(:client).permit(:id, :first_name, :last_name, :email, :contact_num1, :contact_num2, :postal_address, :postal_address_2, :postal_address_3, :company_id)
		end

		def authenticate_owner!
			unless current_user.clients.include?(@client)
				redirect_to welcome_index_path
			end
		end
end
