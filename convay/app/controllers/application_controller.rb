class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :my_colleagues
  before_action :configure_permitted_parameters, if: :devise_controller?

  def my_colleagues
    if current_user
      @my_colleage = User.where("company_id = ? and id != ?", current_user.company.id, current_user.id)
    else
      @my_colleage = nil
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:name, :company_id]

  end

end
