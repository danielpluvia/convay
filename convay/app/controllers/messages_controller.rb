class MessagesController < ApplicationController

  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.send_by current_user.id

    @path = conversation_path(@conversation)
    render 'create.js'
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end
end