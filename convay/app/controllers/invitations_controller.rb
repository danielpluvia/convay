class InvitationsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_invitation, except: [:index, :create, :new]
  before_action :authenticate_owner!, except: [:index, :new, :create]

  layout 'base_panel', :only => :index

  # GET /invitations
  # GET /invitations.json
  def index
    @my_invitations = Invitation.my_invitations current_user.id
    @invitations_to_me = Invitation.invitations_to_me current_user.id
    @invitations = @my_invitations + @invitations_to_me
    @my_invitations = @my_invitations
    @temp_invitation = Invitation.new
    @my_clients = current_user.clients
  end

  # GET /invitations/1
  # GET /invitations/1.json
  def show
  end

  # GET /invitations/new
  def new
    @my_clients = current_user.clients
    @all_companys = Company.all
    # Find all the users of a random company.
    @default_receivers = User.where(company_id: @all_companys.first.id)
    if @default_receivers.length != 0
      # Find all the clients of a random user.
      @default_recever_clients = @default_receivers.first.clients
    end
    @invitation = Invitation.new
  end

  # GET /invitations/1/edit
  def edit
    @my_clients = current_user.clients
  end

  # POST /invitations
  # POST /invitations.json
  def create
    new
    @invitation = Invitation.new invitation_params
    @invitation.sender_id = current_user.id
    @invitation.generate_transaction current_user.id
    @invitation.update_status
    @my_own_transaction = (@invitation.sender_id == @invitation.receiver_id)
  end

  # PATCH/PUT /invitations/1
  # PATCH/PUT /invitations/1.json
  def update
    @success_update = nil
    if params[:invitation][:status] == 'Accept'
      @success_update = @invitation.accept_invitation current_user.id, params[:invitation][:receiver_client]
    end
    if params[:invitation][:status] == 'Reject'
      @success_update = @invitation.reject_invitation current_user.id
    end
    @operation = params[:invitation][:status]
  end

  # DELETE /invitations/1
  # DELETE /invitations/1.json
  def destroy
    @destroy_success = @invitation.cancel_invitation current_user.id
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invitation
      @invitation = Invitation.find params[:id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invitation_params
      params.require(:invitation).permit(:sender, :sender_client_id, :receiver_id, :status, :content, :title, :company).except(:company)
    end


    def authenticate_owner!
      unless current_user.id == @invitation.sender_id or current_user.id == @invitation.receiver_id
        redirect_to welcome_index_path
      end
    end
end


