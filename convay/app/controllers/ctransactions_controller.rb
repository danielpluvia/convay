class CtransactionsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_ctransaction, except: [:index, :create, :new]
  before_action :authenticate_owner!, except: [:index, :new, :create]

  layout 'base_panel', :only => :index

	def index
		@ctransactions = current_user.ctransactions.select{|item| item.staus == 'progressing' and item.invitation.status == 'accepted' if item.invitation}
	  @users= User.all

  end

	def show

	end

	def new
		@ctransaction = Ctransaction.new
	end

	def edit

	end

	def create

	end

  def update
    @update_success = @ctransaction.update(ctransaction_params)
  end

	def destroy
    @delete_success = @ctransaction.delete_transaction
	end

	def add_caction
		@cactions = Caction.all
	end

	def bind_caction
		@caction = Caction.find(params[:caction_id])
		@ctransaction.cactions << @caction
		@ctransaction.save
		redirect_to company_ctransaction_path(@company, @ctransaction)
	end

	private

		def set_ctransaction
			@ctransaction = Ctransaction.find params[:id]
		end

		def authenticate_owner!
			unless @ctransaction.user == current_user
				redirect_to welcome_index_path
			end
		end

		def ctransaction_params
			params.require(:ctransaction).permit(:name, :shared_with_user_name, :role, invitation_attributes:[:property,:id]).except(:shared_with_user_name)
    end
end
