json.array!(@invitations) do |invitation|
  json.extract! invitation, :id, :sender, :sender_client, :receiver, :receiver_client, :status, :content, :title
  json.url invitation_url(invitation, format: :json)
end
