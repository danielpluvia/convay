class Client < ActiveRecord::Base

	belongs_to :user
  has_many :invitations

	# Validation
	validates :first_name, :last_name,
						presence: true,
						length: { in: 1..32 },
						format: {
								with: /\A[A-Z][a-z_]+\Z/,
								message: "Invaliad format for name. Please follow the instructions above."
						}
	validates :email,
						presence: true,
						uniqueness: true,
						length: { in: 1..64 },
						format: {
								with: /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/,
								message: "It's not a valid email address. Please make a double check."
						}
  validates :contact_num1,
			presence: true,
			numericality: true

	validates :contact_num2,
					 presence: false,
					 allow_nil: true,
					 allow_blank: true,
           numericality: true

	validates :postal_address,
			     presence: true

  validates :postal_address_2,
            presence: true

  validates :postal_address_3,
            presence: true

	def full_name
		return self.first_name + ' ' + self.last_name
	end
end
