class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
		:recoverable, :rememberable, :trackable, :validatable
	belongs_to :company
	has_many :clients
	has_many :ctransactions
  has_many :conversations, :foreign_key => :sender_id

  before_save :default_values
  def default_values
    self.company = 3
    company = Company.find(3)
  end

  def unread_convos
    unread_convos = []
    my_convos = Conversation.where("recipient_id = ?", self.id)
    my_convos.each do |convo|
      unread_message_number = 0
      convo.messages.each do |message|
        if !message.read_or_not self.id
          unread_message_number += 1
        end
      end
      if unread_message_number>0
        unread_convos.append convo
      end
    end
    return unread_convos
  end

  def my_convo
    return Conversation.where("recipient_id = ?", self.id)
  end

end
