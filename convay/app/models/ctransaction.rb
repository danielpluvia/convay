class Ctransaction < ActiveRecord::Base
	belongs_to :invitation
	belongs_to :user
  has_many :cactions

  attr_accessor :my_client

	accepts_nested_attributes_for :invitation, :user, :cactions, :allow_destroy => :true

  def shared_with
     invitation = self.invitation
     if invitation.sender_id == self.user.id
       return invitation.receiver
     else
       return invitation.sender
     end
  end

  def shared_with_transaction
    return Ctransaction.where("invitation_id = ? AND id != ?", self.invitation.id, self.id).first
  end

  def shared_with_user_name
    return (self.shared_with).email
  end

  # Logic flaw for own transaction.
  def get_my_client
    invitation = self.invitation
    if invitation.sender_id == self.user.id
      return invitation.receiver_client.full_name if invitation.receiver_client
    else
      return invitation.sender_client.full_name if invitation.sender_client
    end
  end

  def delete_transaction
    self.staus = 'deleted'
    return self.save
  end

  def property
    return self.invitation.property
  end

	def is_vendor?
		if is_vendor == true
			return true
		else
			return false
		end
  end

  # Display the progress of current trasaction.
  def progressing
    total_tasks = self.cactions.length
    finished_task = self.cactions.select{|action| action.state == 0}.length
    if total_tasks == 0
      return 0
    else
      return ((finished_task.to_f / total_tasks.to_f)*100).to_s + '%'
    end
  end

  # Display the progress of shared with transaction.
  def shared_progressing
    if self.check_shared
      shared = self.shared_with_transaction

      total_tasks = shared.cactions.length
      finished_task = shared.cactions.select{|action| action.state == 0}.length
      if total_tasks == 0
        return 0
      else
        return ((finished_task.to_f / total_tasks.to_f)*100).to_s + '%'
      end

    end
  end

  # Check whether the transaction is shared
  def check_shared
    return !(self.invitation.sender_id == self.invitation.receiver_id)
  end

end
