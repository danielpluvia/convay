class Company < ActiveRecord::Base
	has_many :users
	has_many :clients, dependent: :destroy

	validates_uniqueness_of :name
	validate :fields_a_and_b_are_different

	def name_not_contains_keyword
		if self.name == 'None'
			errors.add(:name, 'This company name is invalid.')
		end
	end
end
