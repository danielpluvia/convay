class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates_presence_of :body, :conversation_id, :user_id

  def send_by user_id
    self.user_id = user_id
    self.read = false
    self.save!
  end

  def update_read user_id
    if self.user_id == user_id
      return self.read
    else
      if !self.read
          self.read = true
          self.save!
      end
    end
    return self.read
  end

  def read_or_not user_id
    if self.user_id == user_id
      return true
    else
      return self.read
    end

  end


end
