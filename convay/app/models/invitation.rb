class Invitation < ActiveRecord::Base
  belongs_to :sender, :class_name => 'User'
  belongs_to :receiver, :class_name => 'User'
  belongs_to :sender_client, :class_name => 'Client'
  belongs_to :receiver_client, :class_name => 'Client'

  has_many :ctransactions

  attr_accessor :company
  accepts_nested_attributes_for :sender_client, :receiver_client, :sender, :receiver, :allow_destroy => :true

  validates :title,
            presence: true

  def self.my_invitations user_id
    return Invitation.where('sender_id = :user_id AND receiver_id != :user_id', {:user_id => user_id}).select {|invitation| invitation.status == 'sent' }
  end

  def self.invitations_to_me user_id
    return Invitation.where('sender_id != :user_id AND receiver_id = :user_id', {:user_id => user_id}).select {|invitation| invitation.status == 'sent' }
  end

  def generate_transaction user_id
      ctransaction = Ctransaction.new
      ctransaction.invitation = self
      ctransaction.user_id = user_id
      ctransaction.name = self.title
      ctransaction.save
  end

  def update_status
    if sender_id == receiver_id
      self.status = 'accepted'
    else
      self.status = 'sent'
    end
    self.save
  end

  def ownership
    if self.sender == self.receiver
      return 'own'
    else
      return 'shared'
    end
  end

  def cancel_invitation user_id
      if self.ctransactions.length <= 1 and self.ctransactions.first.user_id == user_id
        self.status = 'deleted'
        self.save
        return true
      else
        return false
      end
  end

  def accept_invitation user_id, client_id
    # User send the invitation to himself.
    if self.receiver_id == self.sender_id
      if self.status == 'sent'
        self.status = 'accepted'
        return self.save
      end
    end
    # Change status.
    if self.status == 'sent' and self.receiver_id == user_id
      self.status = 'accepted'
      # Create related trasaction.
      ctransaction = Ctransaction.new
      ctransaction.invitation = self
      ctransaction.user_id = user_id
      ctransaction.name = self.title
      # Save itself.
      ctransaction.save
      self.receiver_client_id = client_id
      return self.save
    end
    return false
  end

  def reject_invitation user_id
    # Change status.
    if self.status == 'sent'
      self.status = 'rejected'
      return self.save
    end
    return false
  end

end
