class Caction < ActiveRecord::Base
    belongs_to :ctransaction

    validates :action_name,
              presence: true

    def update_state state
      if state == 'done'
        self.state = 0
      end
      if state == 'processing'
        self.state = 1
      end
      return self.save
    end

  def state_display
    if self.state == 0
      return 'Done'
    elsif self == 1
      returen 'Precessing'
    end
  end
end
