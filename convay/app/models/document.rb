class Document < ActiveRecord::Base

  def self.genenrate_template template_name, transaction_id
      transaction = Ctransaction.find transaction_id
      return self.send(template_name.downcase, transaction)
  end

  def self.engagement_letter_template transaction
    filename = "#{Rails.root}/public/Engagement letter template example.docx"
    params = {
        :client_name => transaction.get_my_client,
        :date => Time.new.strftime('%F')

    }
    buffer = DocxTemplater.new.replace_file_with_content(filename, params)
    return buffer.string
  end

end
