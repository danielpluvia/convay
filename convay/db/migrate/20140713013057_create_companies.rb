class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
    	t.string :name
    	t.string :contact
    	t.string :address
    	t.timestamps null: false
    end


    # add_reference :clients, :company, index: true
    # add_reference :users, :company, index: true
    #
    # add_column :actions, :role, :boolean
    # add_column :users, :role, :boolean
    #
    # rename_column :actions, :status, :state
  end
end
