class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
    	t.string :first_name
    	t.string :last_name
    	t.string :email
    	t.string :contact_num1
    	t.string :contact_num2
    	t.string :postal_address
      t.boolean :role
      t.string :property_address

      t.references :user
      t.timestamps null: false
    end
  end
end
