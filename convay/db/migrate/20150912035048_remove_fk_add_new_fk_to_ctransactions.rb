class RemoveFkAddNewFkToCtransactions < ActiveRecord::Migration
  def change
    remove_column :ctransactions, :buyer_id
    remove_column :ctransactions, :vendor_id
    add_reference :ctransactions, :invitations
  end
end
