class AddAddressToClient < ActiveRecord::Migration
  def change
    add_column :clients, :postal_address_2, :string
    add_column :clients, :postal_address_3, :string
  end
end
