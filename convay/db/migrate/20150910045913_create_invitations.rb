class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :status
      t.text :content
      t.text :title

      t.timestamps null: false

      t.references :sender
      t.references :receiver
      t.references :sender_client
      t.references :receiver_client
    end
  end
end
