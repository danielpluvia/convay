class AddAttrToCactions < ActiveRecord::Migration
  def change
    add_column :cactions, :description, :text
    add_reference :cactions, :ctransaction
  end
end
