class AddDisplayNameDocument < ActiveRecord::Migration
  def change
    add_column :documents, :display_name, :string
  end
end
