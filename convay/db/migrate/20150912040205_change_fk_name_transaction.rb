class ChangeFkNameTransaction < ActiveRecord::Migration
  def change
    remove_column :ctransactions, :invitations_id
    add_reference :ctransactions, :invitation
  end
end
