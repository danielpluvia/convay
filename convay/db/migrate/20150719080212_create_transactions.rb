class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
    	t.string :name
        t.references :buyer
        t.references :vendor
        t.boolean :state
        t.datetime :create_date
        t.datetime :finish_date
        t.timestamps null: false
    end

  end
end
